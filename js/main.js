var dsnv = [];

// Init màn hình or khi Reloead
var dataJson = localStorage.getItem("DSNV_LOCAL");
if (dataJson != null) {
  var dataArr = JSON.parse(dataJson);
  for (var i = 0; i < dataArr.length; i++) {
    var item = dataArr[i];
    var nv = new NhanVien(
      item.taiKhoan,
      item.hoTen,
      item.email,
      item.matKhau,
      item.ngayLam,
      item.luongCoBan,
      item.chucVu,
      item.gioLam
    );
    dsnv.push(nv);
  }
  renderDSNV(dsnv);
}

// Thêm nhân viên
document.querySelector("#btnThemNV").onclick = function themNhanVien() {
  var nv = layThongTinTuForm();

  var isValid =
    checkRong("tbTKNV", nv.taiKhoan) &&
    checkTK(nv.taiKhoan) &&
    checkTrungTK(nv.taiKhoan, dsnv);
  isValid = isValid & checkRong("tbTen", nv.hoTen) && checkHoTen(nv.hoTen);
  isValid =
    isValid & checkRong("tbEmail", nv.email) &&
    checkEmail(nv.email) &&
    checkTrungEmail(nv.email, dsnv);
  isValid = isValid & checkRong("tbMatKhau", nv.matKhau) && checkMK(nv.matKhau);
  isValid = isValid & checkRong("tbNgay", nv.ngayLam);
  isValid =
    isValid & checkRong("tbLuongCB", nv.luongcoban) &&
    checkLuong(nv.luongcoban);
  isValid = isValid & checkRong("tbChucVu", nv.chucVu);
  isValid =
    isValid & checkRong("tbGiolam", nv.gioLam) && checkGioLam(nv.gioLam);
  if (isValid) {
    dsnv.push(nv);
    var dataJson = JSON.stringify(dsnv);
    localStorage.setItem("DSNV_LOCAL", dataJson);
  }

  renderDSNV(dsnv);
};

// Xóa nhân viên
function xoaNV(tk) {
  var viTri = -1;
  for (var i = 0; i < dsnv.length; i++) {
    var nv = dsnv[i];
    if (nv.taiKhoan == tk) {
      viTri = i;
      break;
    }
  }
  dsnv.splice(viTri, 1);
  var dataJson = JSON.stringify(dsnv);
  localStorage.setItem("DSNV_LOCAL", dataJson);
  renderDSNV(dsnv);
}

// Sửa nhân viên
function suaNV(tk) {
  var viTri = dsnv.findIndex(function (item) {
    return item.taiKhoan == tk;
  });
  if (viTri != -1) {
    showThongTinLenForm(dsnv[viTri]);
    document.getElementById("tknv").disabled = true;
    return true;
  }
}

// Update nhân viên
document.querySelector("#btnCapNhat").onclick = function capNhatNhanVien() {
  var nv = layThongTinTuForm();
  var isValid = checkRong("tbTen", nv.hoTen) && checkHoTen(nv.hoTen);
  isValid = isValid & checkRong("tbEmail", nv.email) && checkEmail(nv.email);
  isValid = isValid & checkRong("tbMatKhau", nv.matKhau) && checkMK(nv.matKhau);
  isValid = isValid & checkRong("tbNgay", nv.ngayLam);
  isValid =
    isValid & checkRong("tbLuongCB", nv.luongcoban) &&
    checkLuong(nv.luongcoban);
  isValid = isValid & checkRong("tbChucVu", nv.chucVu);
  isValid =
    isValid & checkRong("tbGiolam", nv.gioLam) && checkGioLam(nv.gioLam);
  document.getElementById("tknv").disabled = false;
  var vitri = dsnv.findIndex((item) => {
    return item.taiKhoan == nv.taiKhoan;
  });
  if (vitri !== -1) {
    if (isValid) {
      dsnv[vitri] = nv;
      renderDSNV(dsnv);
      resetForm();
      var dataJson = JSON.stringify(dsnv);
      localStorage.setItem("DSNV_LOCAL", dataJson);
    }
  }
};

// Reset form
function resetForm() {
  document.getElementById("formQLNV").reset();
}

// Bỏ disable field tài khoản
document.querySelector("#btnDong").onclick = function resetTaiKhoan() {
  document.getElementById("tknv").disabled = false;
};
function searchNV() {
  var request = document.getElementById("searchName").value.trim();
  document.getElementById("searchName").value = "";
  var result = dsnv.filter(function (e) {
    return e.xepLoai() === request;
  });
  renderDSNV(result);
}

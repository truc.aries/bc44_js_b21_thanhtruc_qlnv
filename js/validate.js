var showMessage = function (id, message) {
  document.getElementById(id).innerHTML = message;
  document.getElementById(id).style.display = "inline-block";
};
var checkTrungTK = function (taiKhoan, dsnv) {
  var index = dsnv.findIndex((item) => {
    return taiKhoan == item.taiKhoan;
  });
  if (index == -1) {
    showMessage("tbTKNV", "");
    return true;
  } else {
    showMessage("tbTKNV", `<p>Tài khoản đã tồn tại</p>`);
    return false;
  }
};
var checkTrungEmail = function (email, dsnv) {
  var index = dsnv.findIndex((item) => {
    return email == item.email;
  });
  if (index == -1) {
    showMessage("tbEmail", "");
    return true;
  } else {
    showMessage("tbEmail", `<p>Email đã tồn tại</p>`);
    return false;
  }
};
var checkTK = function (taiKhoan) {
  if (/^[0-9]{4,6}$/.test(taiKhoan)) {
    showMessage("tbTKNV", "");
    return true;
  } else {
    showMessage("tbTKNV", "Tài khoản nhân viên chỉ được nhập 4-6 ký số!");
    return false;
  }
};
var checkHoTen = function (hoTen) {
  if (/^[A-Za-z ]+$/.test(hoTen)) {
    showMessage("tbTen", "");
    return true;
  } else {
    showMessage("tbTen", "Họ tên chỉ được nhập chữ!");
    return false;
  }
};
var checkEmail = function (email) {
  const re =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
  if (re.test(email)) {
    showMessage("tbEmail", "");
    return true;
  } else {
    showMessage("tbEmail", "Email không hợp lệ!");
    return false;
  }
};
var checkMK = function (matKhau) {
  const mk = /^(?=.*\d)(?=.*[A-Z])(?=.*\W)[\dA-Za-z\W]{6,10}$/;
  if (mk.test(matKhau)) {
    showMessage("tbMatKhau", "");
    return true;
  } else {
    showMessage(
      "tbMatKhau",
      "Mật khẩu bắt buộc 6-10 ký tự (chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt)"
    );
    return false;
  }
};
var checkLuong = function (luongCoBan) {
  if (luongCoBan >= 1000000 && luongCoBan <= 20000000) {
    showMessage("tbLuongCB", "");
    return true;
  } else {
    showMessage("tbLuongCB", "Lương cơ bản phải từ 1.000.000 - 20.000.000!");
    return false;
  }
};
var checkGioLam = function (gioLam) {
  if (gioLam >= 80 && gioLam <= 200) {
    showMessage("tbGiolam", "");
    return true;
  } else {
    showMessage("tbGiolam", "Số giờ làm trong tháng phải từ 80 - 200!");
    return false;
  }
};
var checkRong = function (idErr, value) {
  if (
    value.length == 0 ||
    value == 0 ||
    value === "" ||
    value === "Chọn chức vụ"
  ) {
    showMessage(
      idErr,
      "Mục này không được để trống! Vui lòng nhập hoặc chọn giá trị khác"
    );
    return false;
  } else {
    showMessage(idErr, "");
    return true;
  }
};

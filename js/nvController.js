// Tạo khuôn
function NhanVien(
  _taiKhoan,
  _hoTen,
  _email,
  _matKhau,
  _ngayLam,
  _luongCoBan,
  _chucVu,
  _gioLam
) {
  this.taiKhoan = _taiKhoan;
  this.hoTen = _hoTen;
  this.email = _email;
  this.matKhau = _matKhau;
  this.ngayLam = _ngayLam;
  this.luongcoban = _luongCoBan;
  this.chucVu = _chucVu;
  this.gioLam = _gioLam;
  this.tinhTongLuong = function () {
    if (this.chucVu == "Giám đốc") {
      return this.luongcoban * 3;
    } else if (this.chucVu == "Trưởng phòng") {
      return this.luongcoban * 2;
    } else {
      return this.luongcoban;
    }
  };
  this.xepLoai = function () {
    if (this.gioLam >= 192) {
      return "Xuất sắc";
    } else if (this.gioLam >= 176) {
      return "Giỏi";
    } else if (this.gioLam >= 160) {
      return "Khá";
    } else {
      return "Trung bình";
    }
  };
}

// Lấy thông tin từ form
function layThongTinTuForm() {
  var taiKhoan = document.getElementById("tknv").value;
  var hoTen = document.getElementById("name").value;
  var email = document.getElementById("email").value;
  var matKhau = document.getElementById("password").value;
  var ngayLam = document.getElementById("datepicker").value;
  var luongCoBan = document.getElementById("luongCB").value * 1;
  var chucVu = document.getElementById("chucvu").value;
  var gioLam = document.getElementById("gioLam").value * 1;

  var nv = new NhanVien(
    taiKhoan,
    hoTen,
    email,
    matKhau,
    ngayLam,
    luongCoBan,
    chucVu,
    gioLam
  );
  return nv;
}

// Render lên màn hình
function renderDSNV(dsnv) {
  var contentHTML = "";

  for (var i = 0; i < dsnv.length; i++) {
    var nv = dsnv[i];

    var contentTr = `<tr>
    <td>${nv.taiKhoan}</td>
    <td>${nv.hoTen}</td>
    <td>${nv.email}</td>
    <td>${nv.ngayLam}</td>
    <td>${nv.chucVu}</td>
    <td>${nv.tinhTongLuong()}</td>
    <td>${nv.xepLoai()}</td>
    <td>
    <button onclick="suaNV(${
      nv.taiKhoan
    })" class="btn btn-info" data-toggle="modal" data-target="#myModal">Sửa</button>
    <button onclick="xoaNV(${
      nv.taiKhoan
    })" class="btn btn-dark mt-1">Xóa</button>   
    </td>
    </tr>    
    `;
    contentHTML += contentTr;
  }
  document.getElementById("tableDanhSach").innerHTML = contentHTML;
}

// Show thông tin lên form
function showThongTinLenForm(nv) {
  document.getElementById("tknv").value = nv.taiKhoan;
  document.getElementById("name").value = nv.hoTen;
  document.getElementById("email").value = nv.email;
  document.getElementById("password").value = nv.matKhau;
  document.getElementById("datepicker").value = nv.ngayLam;
  document.getElementById("luongCB").value = nv.luongCoBan;
  document.getElementById("chucvu").value = nv.chucVu;
  document.getElementById("gioLam").value = nv.gioLam;
}
